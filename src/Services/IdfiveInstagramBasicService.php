<?php

namespace Drupal\idfive_instagram_basic\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Class IdfiveInstagramBasicService.
 */
class IdfiveInstagramBasicService {

  /**
   * @var GuzzleHttp\Client
   */
  protected $client;

  /**
   * Construct the variables.
   */
  public function __construct(Client $client) {
    $this->client = $client;
  }

  /**
   * Public method to return instagram posts.
   */
  public function getInstagramPosts($access_token, $count = 4) {

    // Get cache if it's there. We do this to limit API calls to Instagram, which are capped.
    if (isset($access_token)) {
      $url = "https://graph.instagram.com/me/media?access_token=" . $access_token . "&fields=id,username,caption,media_url,permalink,thumbnail_url&limit=" . $count;

      try {
        $result = json_decode($this->client->get($url)->getBody()->__toString(), TRUE);
      }
      catch (RequestException $e) {
        $message = 'Error connecting to instagram with Access Token: ' . $access_token;
        \Drupal::logger('Instagram')->error($message);
        return;
      }

      if ($result['data']) {
        $results = [];
        $results['user_name'] = $result['data'][0]['username'];
        $results['user_link'] = 'https://instagram.com/' . $result['data'][0]['username'];
        $results['results'] = $result['data'];
        foreach ($results['results'] as $key => $item) {
          $results['results'][$key]['image_url'] = $item['media_url'];
          unset($results['results'][$key]['media_url']);
          if (isset($item['thumbnail_url'])) {
            $results['results'][$key]['image_url'] = $item['thumbnail_url'];
            unset($results['results'][$key]['thumbnail_url']);
          }
        }
        // Send a request for a long lived access token refresh at the same time, to keep them current.
        $this->refreshLongLivedToken($access_token);
        \Drupal::cache()->set('social_feed__instagram--' . $access_token . '--' . $count, $results, time() + 7200);
        return $results;
      }
      else {
        return;
      }
    }
    else {
      return;
    }
  }

  /**
   * Custom function to refresh long lived tokens from the Instagram Basic API.
   *
   * See https://developers.facebook.com/docs/instagram-basic-display-api/reference/refresh_access_token#reading.
   */
  public function refreshLongLivedToken($access_token) {
    $url = "https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=" . $access_token;
    $result = json_decode($this->client->get($url)->getBody()->__toString(), TRUE);
    if ($result['access_token']) {
      $message = 'Token reset request sent to instagram for ' . $access_token . '. Token now expires in: ' . $result['expires_in'] . '.';
      \Drupal::logger('Instagram')->notice($message);
    }
  }

}
