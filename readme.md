# idfive Instagram Basic Display API Service

## About

This module provides a service to interact with the [Instagram Basic Display API](https://developers.facebook.com/docs/instagram-basic-display-api). This is not to be confused with the Instagram Graph API, which is more robust, but requires advanced account configuration via business accounts/etc.

This module provides the service only, no templates or fields, those will need to be created by the developer. We have done it this way, to provide maximum project flexibility. See below for configuration steps.

This Service relies on the user providing a valid [long lived access token](https://developers.facebook.com/docs/instagram-basic-display-api/guides/long-lived-access-tokens#get-a-long-lived-token) from facebook. It is vital that it is long lived (60 days, and can be refreshed) vs. a short term access token (1 hour, cannot be refreshed). We have built in functionality to request refresh of the long lived token with each call.

### Important notes

- Tokens, long lived or not, can still be invalidated by Facebook at any time. This especially happens when things like passwords change.
- We are caching successful API calls for 2 hours, in order to cut down requests, and avoid rate limiting.
- Code has been tested to not fatal error when tokens expire/etc, but will be on developer to enact any messaging/etc on the frontend, or to hide fields/etc.

### Sample results

Response from instagram API is parsed before returning to provide:

- user_name: Takes the username of the first post (presumably they are all same user).
- user_link: Creates link to instagram of user_name.
- Results: Array of posts returned:
  - id: The id of instagram post
  - username: username of post owner
  - caption: Plain text of post caption.
  - permalink: Link to post on Instagram.
  - image_url: This is processed to take either media_url (for images) or thumbnail_url (for videos) from the API return, to normalize this URL to always go to an image.

Example DSM (Drupal Set Message) of Service return ($count = 3):

```
[user_name] => imafrigginrobot
[user_link] => https://instagram.com/imafrigginrobot
[results] => Array
(
  [0] => Array
    (
      [id] => 17846390069027179
      [username] => imafrigginrobot
      [caption] => Thank god for library custodians. Am I right @tetonknittingcompany? #ifthatbooksoverdueiexpectsnacksfromyou
      [permalink] => https://www.instagram.com/p/B-ODdGYjEss/
      [image_url] => https://scontent.xx.fbcdn.net/v/t51.2885-15/90757090_1058299711203457_301631695286693006_n.jpg?_nc_cat=105&_nc_sid=8ae9d6&_nc_ohc=88YzOyNsCD8AX-f1nd5&_nc_ht=scontent.xx&oh=5b2d6f2c7feec3f43ac4967cfdd9e1f8&oe=5EA36825
    )

  [1] => Array
    (
      [id] => 18047351380234739
      [username] => imafrigginrobot
      [caption] => Weird weather today, snowing, to whiteout, to roll clouds, to bronzing. Had one run where I was convinced I was a pro skier, and then the on the next run, I realized I know nothing about skiing. #mindthestormslab #fearandloathinginthenorthfork
      [permalink] => https://www.instagram.com/p/B-DkrB0FMEt/
      [image_url] => https://scontent.xx.fbcdn.net/v/t51.2885-15/90742093_197976744965432_963034845464248313_n.jpg?_nc_cat=107&_nc_sid=8ae9d6&_nc_ohc=u5QQvwzM7ywAX-UsmFk&_nc_ht=scontent.xx&oh=7ef2f3a2118bbac582745df96968d1a7&oe=5EA4A944
    )

  [2] => Array
    (
      [id] => 17851838785918661
      [username] => imafrigginrobot
      [caption] => Nice little lunch break on table shoulder today. #whoeverputabootpackuptableiapplaudyourvigor
      [permalink] => https://www.instagram.com/p/B-DkPMwFoB_/
      [image_url] => https://scontent.xx.fbcdn.net/v/t51.2885-15/90343952_2640587469560297_961793732677186665_n.jpg?_nc_cat=103&_nc_sid=8ae9d6&_nc_ohc=h7AL4He4L_QAX9v0Iy1&_nc_ht=scontent.xx&oh=a01c3a1837d9e06d67e29158c1798376&oe=5EA2C229
    )

)
```

### Service details

- Creates the `idfive_instagram_basic.client` Service in Drupal.
- Results returned via `getInstagramPosts($token, $count)`.
  - $token: The long lived access token.
  - $count: The number of posts to return. Defaults to 4 if nothing passed.

## Installation

- Install via composer: `composer require idfive/idfive_instagram_basic`.
- Update via composer: `composer update idfive/idfive_instagram_basic`.

Install as you would normally install a contributed Drupal module. Visit [drupal.org](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules) for further information.

## Configuration

### Set up Instagram/Facebook accounts, and get long lived access token

This is potentially the hardest step to understand, as Facebook/Instagram have made it possibly "harder than it needs to be", however that is a bit out of our control.

- Follow initial account setup steps in [Getting Started](https://developers.facebook.com/docs/instagram-basic-display-api/getting-started) documentation.
- Once short term access token aquired, exchange it for a [long lived access token](https://developers.facebook.com/docs/instagram-basic-display-api/guides/long-lived-access-tokens#get-a-long-lived-token).
- Once a long lived access token is received, it is valid for 60 days. However, we have added functionality in this module to request extension of that token when calls are made.
- See [refresh a long lived access token](https://developers.facebook.com/docs/instagram-basic-display-api/guides/long-lived-access-tokens#refresh-a-long-lived-token) for details, if desired.

### Add a token field to entity of choice in Drupal

Again, we provide no field in this module to input an access token. In lieu odf adding a field, you could potentially also add directly into Service call, if you have private repository/etc. In that case you would imply pass as a string to the Service, instead of a field value as shown in the example.

- This should be a simple text field, though make sure the maxlength is long, as the tokens are lengthy (255 should be sufficient).
- Could be added to theme settings, if one account to be used site wide, or to nodes, paragraphs, etc.

### Preprocess the entity to get posts

In the entity preprocess of your choice:

- Get access token from field on whatever entity you created it on.
- Set a variable for use in corresponding twig template.
- Be sure this module is enabled, thus the service is available.
- Create new instance of the service.
- Get posts from service, passing the token, and the desired count of posts to return (defaults to 4 if nothing passed).

```php
// Get the token from the field you set up.
$token = $node->THE_ACCESS_TOKEN_FIELD_YOU_ADDED->getValue();
// Set var for use in twig.
$variables['social_instagram'] = NULL;
// Check if module enable, thus service available.
$module_handler = \Drupal::service('module_handler');
if ($module_handler->moduleExists('idfive_instagram_basic')) {
  // Initialize service.
  $instagram_client = \Drupal::service('idfive_instagram_basic.client');
  // Pass token, and desired count.
  $variables['social_instagram'] = $instagram_client->getInstagramPosts($token[0]['value'], 8);
}
```

### Display posts in twig template

twig template example:

```php
{% if social_instagram %}
  <a href="{{ social_instagram.user_link }}" class="link--inherit" target="_blank">@{{ social_instagram.user_name }}</a>
  {% if social_instagram.results %}
    {% for post in social_instagram.results %}
        <a href="{{ post.permalink }}" target="_blank" class="social-feed__post social-feed__post--image">
            <div class="social-feed__post-image" role="img" aria-label="" style="background-image: url({{ post.image_url }})">
              <img src="{{ post.image_url }}" alt="" aria-hidden="true">
            </div>
            <div class="social-feed__post-data">
              <div class="social-feed__post-date">{{ post.date }}</div>
              <p class="social-feed__post-content">{{ post.caption }}</p>
            </div>
        </a>
    {% endfor %}
  {% else %}
    <p>Sorry, there was an error connecting to instagram.</p>
  {% endif %}
{% endif %}
```

Again, above is an example. Exact markup/css/etc will be on the developer to build.

## Options

- Module provides no configurable options. All tokens/etc will need to be added by developer, to entity of choice/etc.

## Development

- No special development considerations.
- See [developers.idfive.com](https://developers.idfive.com/), and [drupal documentation standards](https://www.drupal.org/docs/develop/documenting-your-project/module-documentation-guidelines).
